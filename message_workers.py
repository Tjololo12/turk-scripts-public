import sys
import boto3
import getopt


def show_help():
    print("Usage:\n"
          "  (-i|--id=) Qualification type ID for users\n"
          "  (-w|--workers=) Comma-separated list of IDs to notify (cannot be used with -i)\n"
          "  (-s|--subject=) Subject of email\n"
          "  (-b|--body=) Body of email. For multiline, use $'' syntax which parses \\n properly\n"
          "  (-e|--environment=) Prod for production, defaults to sandbox\n"
          "  (-p|--profile=) Name of credential profile\n"
          "  [-t (Test mode) -h (print this help text)]")
    sys.exit(2)


def main(argv):
    profile_name = ''
    qualification_id = ''
    workers_list = []
    subject = ''
    body = ''
    body_list = []
    test = False
    page = 0
    environments = {
        "live": {
            "endpoint": "https://mturk-requester.us-east-1.amazonaws.com",
            "preview": "https://www.mturk.com/mturk/preview",
            "manage": "https://requester.mturk.com/mturk/manageHITs",
            "reward": "0.00"
        },
        "sandbox": {
            "endpoint": "https://mturk-requester-sandbox.us-east-1.amazonaws.com",
            "preview": "https://workersandbox.mturk.com/mturk/preview",
            "manage": "https://requestersandbox.mturk.com/mturk/manageHITs",
            "reward": "0.11"
        },
    }
    hostenv = environments["sandbox"]
    try:
        opts, args = getopt.getopt(argv, "thi:s:b:e:p:w:",
                                   ["workers=", "subject=", "body=", "environment=", "profile=", "id="])
    except getopt.GetoptError:
        show_help()
        opts, args = ([], [])

    for opt, arg in opts:
        if opt == '-h':
            show_help()
        elif opt == '-t':
            test = True
            print("Test Mode")
        elif opt in ("-e", "--environment"):
            if arg.lower() == "prod":
                hostenv = environments["live"]
            else:
                hostenv = environments["sandbox"]
        elif opt in ("-i", "--id"):
            qualification_id = arg
        elif opt in ("-s", "--subject"):
            subject = arg
        elif opt in ("-b", "--body"):
            body = arg
        elif opt in ("-w", "--workers"):
            workers_list = arg.split(",")
        elif opt in ("-p", "--profile"):
            profile_name = arg
        else:
            show_help()
    if qualification_id == '' and len(workers_list) == 0:
        print("Qualification Type ID or list of workers is required")
        sys.exit(2)
    if profile_name == '':
        print("Profile is required")
        sys.exit(2)
    session = boto3.Session(profile_name=profile_name)
    client = session.client(
        service_name='mturk',
        region_name='us-east-1',
        endpoint_url=hostenv['endpoint'],
    )
    workers_to_notify = []
    if qualification_id != '':
        response = client.list_workers_with_qualification_type(QualificationTypeId=qualification_id, MaxResults=100,
                                                               Status="Granted")
        if test:
            print(response)
        quals = []
        quals.extend(response["Qualifications"])
        while response["NumResults"] >= 100:
            if test:
                print(response)
            workers_to_notify.extend(response["Qualifications"])
            page += 1
            print("More than 100 workers, attempting to get page " + str(page))
            token = response['NextToken']
            response = client.list_workers_with_qualification_type(QualificationTypeId=qualification_id, MaxResults=100,
                                                                   NextToken=token, Status="Granted")
        if test:
            print(response)
        workers_to_notify.extend(response["Qualifications"])
        for item in quals:
            workers_to_notify.append(item["WorkerId"])
    for worker in workers_list:
        if worker not in workers_to_notify:
            workers_to_notify.append(worker.strip())
    workers_formatted = []
    for worker in workers_to_notify:
        wid = ""
        if "WorkerId" in worker:
            wid = worker["WorkerId"]
        else:
            wid = worker
        if wid not in workers_formatted:
            workers_formatted.append(wid)
    workers_formatted.sort()
    input("Send message to " + str(len(workers_formatted)) + " workers?")
    if test:
        print("Workers to notify: " + str(workers_formatted))
        print("Subject: " + subject)
        print("Body: " + body)
    else:
        final_list = [workers_formatted[i:i + 100] for i in range(0, len(workers_formatted), 100)]
        for chunk in final_list:
            response = client.notify_workers(Subject=subject, MessageText=body, WorkerIds=chunk)
            print(response)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        show_help()
    main(sys.argv[1:])
