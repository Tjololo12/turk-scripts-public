# Mechanical Turk Requester Scripts
## Background

This github includes a sampling of the scripts that I've written. I'm releasing some of them for free because I feel they'd be useful for everyone to have access to them. This is not all of the scripts I've written, but as some of them are offered for sale I am not sharing the source here. However, these scripts should give a good enough baseline to be able to modify them for use for any of the Mechanical Turk Python SDK functions.

## Prerequisites

```
Python 3
boto3
awscli
```

## Initial Setup

1. Requester requirements are listed [here](https://docs.aws.amazon.com/AWSMechTurk/latest/AWSMechanicalTurkGettingStartedGuide/SetUp.html). This link describes the steps required to create an AWS account and link your requester account to it for API manipulation.

2. In order to configure profiles for your scripts, you should install the `awscli` package for your distribution. In your `~/.aws/credentials` file, you should add your key and secret key you generated in the AWS console while doing step 1. It should look something like this
```
[profilename]
aws_access_key_id=<insert access key here>
aws_secret_access_key=<insert secret key here>
```

## Utility

Most scripts are designed to be run as `python scriptname.py` for general usage info (also accepts the `-h` switch for help info). That will give you an idea of what the script accepts argument-wise.

### Script List - Public

The public archive contains the following scripts:

* `message_workers.py`: Designed to take a list of users (either who have a specific qualification, are passed in on the commandline, or both) and send them an email. Note that you cannot message workers who have never completed an actual HIT from you.
* `delete_qualification.py`: This script deletes a qualification given on the commandline.
* `checkBalance.py`: A simple script to verify your environment is configured properly. It should return your current account balance. If there is an issue, the script will return the error.

### Script List - Private

The following scripts are ones I have written but do not offer for public use. You may contact me if you wish to discuss purchasing a script or paying me to use one (or more) scripts for you.

* `assign_qualification.py`: Assigns qualification given on the commandline to one or more users from either the results of a HIT, listed on the commandline, or both, with a specific value.
* `create_qualification.py`: Create a qualification as specified by question/answer file using standard mturk schema
* `grant_qualifications.py`: Retrieve submitted qualifications, check the free-text response, and grant qualification if if matches (or reject with customizable reason)
* `revoke_qualification.py`: Remove qualification from workers given by either input file (such as HIT results), listed on the commandline, or both, with customizable reason.
* `check_quals.py`: Lists worker IDs with specific qual. Supports searching for specific worker, returning their value if they exist.
* `check_qual_values.py`: Designed to change the value of a specific qualification for a list of users given by an input file (either HIT results or a custom created file with the proper format)
* `post_hit`: A folder containing code and example configs to post an external HIT using the boto3 API
* Custom scripts: As use cases come up this list may change with one-offs or custom creations. I'm willing to code scripts (within my ability) for custom use cases as well.

## Too difficult?

If this seems like too much work and you don't have the motivation or technical understanding to feel comfortable about it, I also work as a consultant/script manager for requesters. Contact me for more information.

### Contact information

email: requester (at) ns4t.net
