import sys
import boto3
import getopt


def show_help():
    print("Usage:\n"
          "  (-e|--environment=) Prod for production, defaults to sandbox\n"
          "  (-p|--profile=) Name of credential profile\n"
          "  [-h (print this help text)]\n"
          "  The purpose of this script is to verify you have your environment configured properly.")
    sys.exit(2)


def main(argv):
    profile_name = ''
    environments = {
        "live": {
            "endpoint": "https://mturk-requester.us-east-1.amazonaws.com",
            "preview": "https://www.mturk.com/mturk/preview",
            "manage": "https://requester.mturk.com/mturk/manageHITs",
            "reward": "0.00"
        },
        "sandbox": {
            "endpoint": "https://mturk-requester-sandbox.us-east-1.amazonaws.com",
            "preview": "https://workersandbox.mturk.com/mturk/preview",
            "manage": "https://requestersandbox.mturk.com/mturk/manageHITs",
            "reward": "0.11"
        },
    }
    hostenv = environments["sandbox"]
    try:
        opts, args = getopt.getopt(argv, "the:p:", ["environment=", "profile="])
    except getopt.GetoptError:
        show_help()
        opts, args = ([], [])

    for opt, arg in opts:
        if opt == '-h':
            show_help()
        elif opt in ("-e", "--environment"):
            if arg.lower() == "prod":
                hostenv = environments["live"]
            else:
                hostenv = environments["sandbox"]
        elif opt in ("-p", "--profile"):
            profile_name = arg
        else:
            show_help()
    if profile_name == '':
        print("Profile is required")
        sys.exit(2)

    session = boto3.Session(profile_name=profile_name)
    client = session.client(
        service_name='mturk',
        region_name='us-east-1',
        endpoint_url=hostenv['endpoint'],
    )
    try:
        response = client.get_account_balance()
        print("Available balance: %s" % response['AvailableBalance'])
    except client.exceptions.RequestError as e:
        print("There was some error: %s" % e)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        show_help()
    main(sys.argv[1:])
